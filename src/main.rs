use clap::Parser;

const ARR_SIZE: usize = 10;

#[derive(Parser)]
struct Cli {
	/// File path of a Brainfuck program
	file: std::path::PathBuf,
}

fn compile(contents: String) -> [u8; ARR_SIZE] {
	let mut memory: [u8; ARR_SIZE] = [0; ARR_SIZE];
	let mut ptr: usize = 0;
	let mut is_looping = false;
	let mut loop_stack: Vec<usize> = Vec::new();
	let mut inner_loops = 0;
	
	for char in contents.chars() {
		if is_looping {
			if char == '[' {
				inner_loops += 1;
			}
			if char == ']' {
				if inner_loops == 0 {
					is_looping = false;
				} else {
					inner_loops -= 1;
				}
			}
			continue;
		}
		
		match char {
			'+' => memory[ptr] += 1,
			'-' => memory[ptr] -= 1,
			'>' => ptr += 1,
			'<' => ptr -= 1,
			'.' => print!("{}", memory[ptr] as char),
			'[' => {
				if memory[ptr] == 0 {
					is_looping = true;
				} else {
					// do something
				}
			}
			_ => {}
		}
	}

	memory
}

fn main() {
	let args = Cli::parse();
    println!("Brainfuck compiler written in Rust");
	println!("Compiling {}...", args.file.to_string_lossy());

	let contents = std::fs::read_to_string(args.file).expect("[error] Unable to open the file!");
	let memory = compile(contents);
	println!("{:?}", memory);
	
	println!("");
}